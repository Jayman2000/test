extends Area2D

export(int) var health = 1
var explosionScene = preload("res://scenes/Explosion.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Hazard_area_entered(area):
	health -= 1
	if health > 0:
		$Sprite.modulate = Color(1, 0, 0)
		$ModulationTimer.start()
	else:
		queue_free()
	
		var explosion = explosionScene.instance()
		explosion.position = position
		get_parent().add_child(explosion)


func _on_ModulationTimer_timeout():
	$Sprite.modulate = Color(1, 1, 1)
