extends Node2D

var hasAnimationFinished
var hasAudioFinished

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimatedSprite.play()
	$AudioStreamPlayer.play()
	
	hasAnimationFinished = false
	hasAudioFinished = false


func _on_AnimatedSprite_animation_finished():
	$AnimatedSprite.hide()
	hasAnimationFinished = true
	freeWhenFinished()

func _on_AudioStreamPlayer_finished():
	hasAudioFinished = true
	freeWhenFinished()

func freeWhenFinished():
	if hasAnimationFinished and hasAudioFinished:
		queue_free()


