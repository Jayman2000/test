extends Area2D

export(float) var speed = 500
var explosionScene = preload("res://scenes/Explosion.tscn")
var bulletScene = preload("res://scenes/PlayerLaser.tscn")

export(float) var shootDelay = 1
var secondsSinceLastShot

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimatedSprite.set_frame(0)
	secondsSinceLastShot = shootDelay

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# Movement
	var velocity = Vector2(0, 0) # Only counts velocity applied by player input
	
	if Input.is_action_pressed("ui_left"):
		velocity.x -= speed
	if Input.is_action_pressed("ui_right"):
		velocity.x += speed
	if Input.is_action_pressed("ui_up"):
		velocity.y -= speed
	if Input.is_action_pressed("ui_down"):
		velocity.y += speed
	
	if velocity.length() > speed:
		velocity = velocity.normalized() * speed
	
	velocity *= delta # Changes velocity's unit from px/sec to just px
	position += velocity
	
	# Prevent the player from moving off screen
	var viewport_rect = get_viewport_rect()
	position.x = clamp(position.x, viewport_rect.position.x, viewport_rect.end.x)
	position.y = clamp(position.y, viewport_rect.position.y, viewport_rect.end.y)
	
	# Make the ship look like it's tilting when moved up and down
	if velocity.y < 0:
		$AnimatedSprite.set_frame(1)
	elif velocity.y > 0:
		$AnimatedSprite.set_frame(2)
	else:
		$AnimatedSprite.set_frame(0)
	
	# Shooting
	secondsSinceLastShot += delta
	if Input.is_action_pressed("shoot") and secondsSinceLastShot >= shootDelay:
		shoot()
		secondsSinceLastShot = 0

func shoot():
	var bullet = bulletScene.instance()
	bullet.position = position
	get_parent().add_child(bullet)

func _on_Player_area_entered(area):
	$AnimatedSprite.hide()
	$AnimatedSprite.set_frame(0)
	set_process(false)
	
	var explosion = explosionScene.instance()
	explosion.position = position
	get_parent().add_child(explosion)
