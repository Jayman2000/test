extends Area2D

export(float) var speed = 100

# Called when the node enters the scene tree for the first time.
func _ready():
	$SpawnSound.play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position.x += speed * delta

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()


func _on_PlayerLaser_area_entered(area):
	queue_free()
