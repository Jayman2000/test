extends "res://scripts/Hazard.gd"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export(float) var speed = 100
export(float) var frequency = 1
var vertTimer = 0
var up = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	vertTimer += delta
	if vertTimer >= frequency * 2:
		vertTimer = 0
		up = not up
	
	if vertTimer >= frequency:
		if up:
			position.y += speed * delta
		else:
			position.y -= speed * delta
	else:
		position.x -= speed * delta
